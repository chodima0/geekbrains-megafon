### GeekBrains Megafon Course

---

__name__: Cho Hyun Woo  
__e-mail__: chodima0@gmail.com

---

##### Lessons' table of content:

1. Lesson 1. Linear regression, L1/L2 baseline.
2. Lesson 2. Shapely & Geopandas.
3. Lesson 3. CV & Quantile regression & Error prediction.
4. Lesson 4-6. Pipeline.
5. Lesson 7. Luigi.
6. Lesson 8. Luigi examples.
